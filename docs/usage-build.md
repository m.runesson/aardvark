# Usage of build pipeline

This document describes how to set up a project to take advantage of Aardvark to
build it and delivers the build artifact(container image) to a Docker registry.

If you not already have a repository in Gitlab, create it. Default branch must be `main` or `master`.

Create a `Dockerfile` in the top directory of your repository.
Configure it to build with a single `docker build .` or `podman build .` command.

**Tip:** If you are working with a compiled language it is preferable to take advantage
of [multi-stage builds](https://docs.docker.com/develop/develop-images/multistage-build/)
to make your images as small as possible in the end.

Ensure the `Dockerfile` exist on your default branch(`main` or `master`).

Go to the GitLab page for the repository. Under *Settings*, select *Webhooks*.
In the field *url* set the webhook url you got from your administrator.

Field *Secret Token* is the webhook secret you got from your administrator.

Mark the checkboxes to trigger the webhook for the following events:

- Push events
- Tag push events
- Merge request events

Finally click *Add webhook*.

Test your setup selecting *Test* and *Push event* for the webhook just created.
This shall trigger a build in your pipeline and result in an image pushed to your
docker registry named after your repository and tagged

Once the build is working, move on to
[setup deploy on the same cluster](usage-deploy.md).
